﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MiniJSON;
using System;


public class ButtonSc : MonoBehaviour {
	public GameObject timer;
	public GameObject win;
	static int QuestionNo = 0;


	int questionCount = 1;

	public Text questionText;
	public Text[] optionText;


	public GameObject[] options;

	public List<object> list = new List<object>();
	public Dictionary <string, object> dic = new Dictionary<string, object>();

	public int questionNo = 0;


	void Start () {
		timer = GameObject.FindGameObjectWithTag ("timer");
		timer.GetComponent<Timer> ();


		StartCoroutine (DownloadData());
	}

	IEnumerator DownloadData()
	{
		

		string dataInfoDownLoadURL = "http://infiniqo.api.nbos.io/api/trivia/questions";

		WWW request = new WWW (dataInfoDownLoadURL);

		yield return request;


		list = Json.Deserialize (request.text) as List <object>;

		NextQuestion ();

	}


	public void OnButtonPress()
	{	 
		win.SetActive (true);
		StartCoroutine (delay());
	}
		
	IEnumerator delay()
	{

		yield return new WaitForSeconds (2);
		win.SetActive (false);
		timer.GetComponent<Timer> ().min = 60f;
		NextQuestion ();

	}

	public void NextQuestion()
	{

		if(list.Count == questionNo)
		{
			questionNo = 0;
		}
		dic = list [questionNo] as Dictionary <string, object>;

		questionText.text = Convert.ToString (dic["title"]);



	
		List <object> list1 = dic ["options"] as List <object>;

		Dictionary <string, object> dic1 = new Dictionary<string, object> ();


		for(int i =0;i<list1.Count;i++)
		{

			dic1 = list1 [i] as Dictionary <string, object>;
			optionText[i].text = Convert.ToString (dic1["label"]);
			options [i].GetComponent<optionID> ()._OID = Convert.ToString (dic1["_id"]);
		}



		questionNo++;
	}
//	IEnumerator UploadData()
//	{
//		string dataInfoUploadURL = "";
//
//	}
}



