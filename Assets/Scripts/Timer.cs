﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
    bool Zero = false;
	public Text timer;
	public float min = 60f;
    float timerReset = 60f;
	public int questionCount=0;

	void Start () {
        min = timerReset;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!Zero) {
			min -= Time.deltaTime;
			timer.text = min.ToString ("f2");
			if (min <= 0) 
			{
				Zero = true;
				timer.text = "0";
				StartCoroutine(TimerDelay ());
			}
		} 
    }

    IEnumerator TimerDelay()
    {
		questionCount++;
		yield return new WaitForSeconds (3);
		min = timerReset;
		Zero = false;
    }
}
