﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour {

	public GameObject Question;
	public GameObject option1;
	public GameObject option2;
	public GameObject option3;
	public GameObject option4;
	public GameObject option5;

	void Start () {
		StartCoroutine (run());
	}


	IEnumerator run()
	{
		yield return new WaitForSeconds (3);
		Question.SetActive (true);
		yield return new WaitForSeconds (2);
		option1.SetActive (true);
		yield return new WaitForSeconds (2);
		option2.SetActive (true);
		yield return new WaitForSeconds (2);
		option3.SetActive (true);
		yield return new WaitForSeconds (2);
		option4.SetActive (true);
		yield return new WaitForSeconds (2);
		option5.SetActive (true);
	}


}
