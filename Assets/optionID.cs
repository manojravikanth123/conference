﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MiniJSON;
using System;
using UnityEngine.Networking;

public class optionID : MonoBehaviour {

	public string _OID = null; 
	public string _QID = null;

	public string jsonData;

	public GameObject canvas;

	public Button btn;

	public List<object> list = new List<object>();

	void Start () {
		btn.onClick.AddListener (TaskOnClick);
	}
	
	// Update is called once per frame
	void Update () {
		_QID = Convert.ToString(canvas.GetComponent<ButtonSc>().dic["_id"]); 
	}
	void TaskOnClick()
	{
		list.Add (_OID);
		list.Add (_QID);
		jsonData = Json.Serialize (list);

		StartCoroutine (Upload());
	}

	IEnumerator Upload()
	{
		string dataUploadLink = "http://infiniqo.api.nbos.io/api/trivia/5b34bfea38cbe80010b642a6/responses";

		UnityWebRequest www = UnityWebRequest.Post (dataUploadLink, jsonData);

		yield return www.SendWebRequest ();
	}

}
